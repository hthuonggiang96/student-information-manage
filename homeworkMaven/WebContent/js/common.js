function showMessageErr(e, idMess, mess) {
    $(e).before("<div class='alert alert-danger' role='alert' id='"+idMess+"'>"+mess+"</div>");
    setTimeout("$('#"+idMess+"').alert('close');", 4000);
}
function showMessageSuccess(e, idMess, mess) {
    $(e).before("<div class='alert alert-primary' role='alert' id='"+idMess+"'>"+mess+"</div>");
    setTimeout("$('#"+idMess+"').alert('close');", 4000);
}
function isLetterVi(str) {
    return /^[a-zA-Z ()ăâđôơư]+$/.test(str);
};
function isLetter(str) {
    return /^[a-zA-Z0-9 ()]+$/.test(str);
};
function isLetterNotSpace(str) {
    return /^[a-zA-Z0-9()]+$/.test(str);
};
function isSpace(str) {
    return (/\s+/).test(str);
};
function isEmail(str) {
    return /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/.test(str);
};
function isDate(str) {
    return /^(3[01]|[12][0-9]|0[1-9])\/(1[0-2]|0[1-9])\/[0-9]{4}$/.test(str);
    // return /^(0?[1-9]|[12][0-9]|3[01])[\\/\\-](0?[1-9]|1[012])[\\/\\-]\\d{4}$/.test(str);
    // return /^((0?[1-9]|[12][09]|3[01])\/([13578]|0[13578]|1[02])\/(19[0-9]{2}|20[0-9]{2}))||((0?[1-9]|[12][0-9]|30)\/([469]|0[469]|11)\/(19[0-9]{2}|20[1-9]{2}))|((0?[1-9]|[12][0-9])\/(02|2)\/(19[0-9]{2}|20[0-9]{2}))$/.test(str);
};