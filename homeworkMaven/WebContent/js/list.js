var table;
$(document).ready(() => {
	$(".datepicker").datepicker({
		clearBtn: true,
		format: "dd/mm/yyyy"
	}).on('changeDate', function(e){
	    $(this).datepicker('hide');
	});
	$(".area-search").hide();
	$(".area-export").hide();
	$(".area-exp-date").hide();
	$(".area-exp-class").hide();
	$("#btn-search").click(() => {
		$(".area-search").slideToggle();
		$(".area-export").hide();
	});
	$("#btn-export").click(() => {
		$(".area-export").slideToggle();
		$(".area-search").hide();
	});
	$(".selectpicker").change(function() {
		if ($(this).val() === "exp-createdDate") {
			$(".area-exp-date").slideDown();
			$(".area-exp-class").slideUp();
			$("#expClazz").val("");
		}else{
			$(".area-exp-date").slideUp();
			$(".area-exp-class").slideDown();
			$("#expDateFrom").val("");
			$("#expDateTo").val("");
		}
	});
	table = $('#tbStaff').DataTable({
		bDestroy: true,
		bSort: false,
		bFilter: false,
		lengthMenu: [3, 5, 10]
	});
	
	
	$("#btn-search-table").click(function(){
		var lengthMenuSetting = table.page.info().length;
		console.log();
		$.ajax({
		  method: "POST",
		  url: "/homeworkMaven/ListServlet",
		  data: { 
			  searchClazz: $("#searchClazz").val(), 
			  searchAcc: $("#searchAcc").val(),
			  searchDateFrom: $("#searchDateFrom").val(),
			  searchDateTo:$("#searchDateTo").val()
		  }
		})
		  .done(function( response ) {
			  $('#tbStaff').dataTable().fnDestroy();
			  $("#table-body").children().remove();
			  $("#table-body").append(response);
			  table = $('#tbStaff').DataTable({
				  bDestroy: true,
				  bSort: false,
				  bFilter: false,
				  lengthMenu: [3, 5, 10],
				  iDisplayLength: lengthMenuSetting
			  });
		  });
	});
	
})
function del(e){
		var page = table.page();
		var lengthMenuSetting = table.page.info().length;
		$.ajax({
		  method: "POST",
		  url: "/homeworkMaven/delete",
		  data: { 
			  id: $(e).next().val(),
			  searchClazz: $("#searchClazz").val(), 
			  searchAcc: $("#searchAcc").val(),
			  searchDateFrom: $("#searchDateFrom").val(),
			  searchDateTo:$("#searchDateTo").val()
		  }
		})
		  .done(function( response ) {
			  $('#tbStaff').dataTable().fnDestroy();
			  $("#table-body").children().remove();
			  $("#table-body").append(response);
			  table = $('#tbStaff').DataTable({
				  bDestroy: true,
				  bSort: false,
				  bFilter: false,
				  lengthMenu: [3, 5, 10],
				  iDisplayLength: lengthMenuSetting
			  });
			  table.page(page).draw(false);
		  });
	}