$(document).ready(function () {
	$(".datepicker-graduation").datepicker({
		clearBtn: true,
		format: "yyyy",
		viewMode: "years", 
    	minViewMode: "years"
	}).on('changeDate', function(e){
	    $(this).datepicker('hide');
	});
	$(".datepicker").datepicker({
		clearBtn: true,
		format: "dd/mm/yyyy"
	}).on('changeDate', function(e){
	    $(this).datepicker('hide');
	});
	
	$("#btn-submit").click(function(){
		if (validate() === true) {
			$("#info-form").submit();
		}
	});

	function validate(){
		var check = true;
		if (!checkRequried("class", "class-lbl") || !checkNotSpace("class", "class-lbl")) {
			check = false;
		}
		if (!checkRequried("name", "name-lbl")) {
			check = false;
		}
		if (!checkRequried("graduation-year", "graduation-year-lbl") || !checkNotSpace("graduation-year", "graduation-year-lbl")) {
			check = false;
		}
		if (!checkRequried("sex option:selected", "sex-lbl")) {
			check = false;
		}
		if (!checkRequried("devtest option:selected", "devtest-lbl")) {
			check = false;
		}
		if (!checkRequried("school option:selected", "school-lbl")) {
			check = false;
		}
		if (!checkRequried("specialize", "specialize-lbl")) {
			check = false;
		}
		if (!checkRequried("job option:selected", "job-lbl")) {
			check = false;
		}
		if (!checkRequried("nationality", "nationality-lbl")) {
			check = false;
		}
		if (!checkRequried("birthday", "birthday-lbl") || !checkNotSpace("birthday", "birthday-lbl") || !checkDate("birthday", "birthday-lbl")) {
			check = false;
		}
		if (!checkNotSpace("graduation-date", "graduation-date-lbl") || !checkDate("graduation-date", "graduation-date-lbl")) {
			check = false;
		}
		if (!checkRequried("placeOfBirth option:selected", "placeOfBirth-lbl")) {
			check = false;
		}
		if (!checkRequried("level option:selected", "level-lbl")) {
			check = false;
		}
		if (!checkRequried("address", "address-lbl")) {
			check = false;
		}
		if (!checkRequried("cmnd", "cmnd-lbl")) {
			check = false;
		}
		if (!checkRequried("cmnd-date", "cmnd-date-lbl") || !checkNotSpace("cmnd-date", "cmnd-date-lbl") || !checkDate("cmnd-date", "cmnd-date-lbl")) {
			check = false;
		}
		if (!checkRequried("placeOfCMND option:selected", "placeOfCMND-lbl")) {
			check = false;
		}
		if (!checkRequried("acc", "acc-lbl") || !checkNotSpace("acc", "acc-lbl")) {
			check = false;
		}
		if (!checkRequried("mail", "mail-lbl") || !checkNotSpace("mail", "mail-lbl") || !checkMail("mail", "mail-lbl")) {
			check = false;
		}
		if (!checkRequried("phoneNumber", "phoneNumber-lbl") || !checkNotSpace("phoneNumber", "phoneNumber-lbl")) {
			check = false;
		}
		return check;
	}

	function checkRequried(idInp, idLabel){
		var check = true;
		var val = $("#"+idInp).val();
		if (val.trim() == null || val.trim() == "") {
			showMessageErr("#"+idLabel, idLabel+"Mess", "Không được để trống");
			check = false;
		}
		return check;
	}
	
	function checkNotSpace(idInp, idLabel){
		var check = true;
		var val = $("#"+idInp).val();
		console.log(val)
		console.log(isSpace(val))
		if (isSpace(val)) {
			showMessageErr("#"+idLabel, idLabel+"Mess", "Không được có dấu cách giữa các từ/kí tự.");
			check = false;
		}
		return check;
	}
	function checkDate(idInp, idLabel){
		var check = true;
		var val = $("#"+idInp).val();
		if ((val.trim() !== null && val.trim() !== "") && !isDate(val)) {
			showMessageErr("#"+idLabel, idLabel+"Mess", "Sai định dạng ngày (dd/mm/yyyy)");
			check = false;
		}
		return check;
	}
	function checkMail(idInp, idLabel){
		var check = true;
		var val = $("#"+idInp).val();
		if ((val.trim() !== null && val.trim() !== "") && !isEmail(val)) {
			showMessageErr("#"+idLabel, idLabel+"Mess", "Sai định dạng mail");
			check = false;
		}
		return check;
	}

})