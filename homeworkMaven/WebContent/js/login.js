$(document).ready(function () {
	$("#btn-admin").click(function () {
		var username = $("#username").val();
		var password = $("#password").val();
		if (isLetter(username) == false || isLetter(password) == false) {
			showMessageErr(".form", "nullErr", "Username hoặc password không được để trống<br>Giữa các từ không được có dấu cách");
			return;
		}
		$("form").submit();
	})
})