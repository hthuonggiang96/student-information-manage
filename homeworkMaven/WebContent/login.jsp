<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/login.css">
	</head>
	<body>
		<div class="container container-more">
			<!-- Content here -->
			<div class="login">
				<c:if test="${(not empty message) && fn:length(message) > 0 }">
					<p class="error">${message }</p>
				</c:if>
				<div class="form">
					<form action="login" method="post">
						<div class="form-group">
							<input type="text" class="form-control" id="username" name="username" placeholder="Username" size="50">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="password" name="password" placeholder="Password">
						</div>
						<div>
						<button type="button" class="btn btn-info" id="btn-admin">Login</button>
						</div>
						<div class="form-group label">
							<a href="index.jsp" class="text-i"><i>Tôi không phải là Admin !!!</i></a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script src="bootstrap/ajax/jquery/jquery.min.js"></script>
		<script src="bootstrap/ajax/popper/popper.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="js/common.js"></script>
		<script src="js/login.js"></script>
	</body>
</html>