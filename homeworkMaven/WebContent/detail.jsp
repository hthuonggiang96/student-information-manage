<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Nhân viên</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.css">
		<link rel="stylesheet" href="bootstrap/css/bootstrap-select.min.css">
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" type="text/css" href="css/detail.css">
	</head>
	<body>
		<div class="container">
			<!-- Content here -->
			<%
		        if(session != null && session.getAttribute("username") != null)  {
		    %>  
			<div class="header">
				<div class="info-login text-primary font-weight-bold"><p><%=session.getAttribute("username") %></p></div>
				<a href="logout" type="button" class="btn btn-dark btn-sm"
				title="logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
			</div>
			<%
	        	}
			%>
			<div class="info-detail">
				<c:if test="${(not empty message) && fn:length(message) > 0 }">
					<p class="error">${message }</p>
				</c:if>
				<div class="form">
					<form id="info-form" action="createEdit" method="post">
						<c:if test="${not empty stf }">
							<input type="hidden" class="form-control" id="id" name="id" value="${stf.id }">
						</c:if>
						<div class="form-group">
							<label id="class-lbl">Lớp<span class="required-label">*</span></label>
							<c:if test="${not empty stf }">
								<input type="text" class="form-control" id="class" name="class" value="${stf.clazz }">
							</c:if>
							<c:if test="${empty stf }">
								<input type="text" class="form-control" id="class" name="class">
							</c:if>
						</div>
						<div class="form-group">
							<label id="code-lbl">Mã nhân viên</label>
							<c:if test="${not empty stf }">
								<input type="text" class="form-control" id="code" name="code" value="${stf.code }">
							</c:if>
							<c:if test="${empty stf }">
								<input type="text" class="form-control" id="code" name="code">
							</c:if>
						</div>
						<div class="form-group">
							<label id="name-lbl">Họ và tên<span class="required-label">*</span></label>
							<c:if test="${not empty stf }">
								<input type="text" class="form-control" id="name" name="name" value="${stf.name }">
							</c:if>
							<c:if test="${empty stf }">
								<input type="text" class="form-control" id="name" name="name">
							</c:if>
						</div>
						<div class="form-group">
							<label id="graduation-year-lbl">Năm tốt nghiệp<span class="required-label">*</span></label>
							<div class="datepicker-graduation date input-group p-0">
								<div class="input-group-append">
									<span class="input-group-text date-label"><i class="fa fa-calendar"></i></span>
								</div>
								<c:if test="${not empty stf }">
									<input type="text" class="form-control" id="graduation-year" name="graduationyear" value="${stf.graduationYear }">
								</c:if>
								<c:if test="${empty stf }">
									<input type="text" class="form-control" id="graduation-year" name="graduationyear" placeholder="Nếu chưa tốt nghiệp, hãy điền năm dự kiến tốt nghiệp cao nhất">
								</c:if>
							</div>
						</div>
						<div class="form-group">
							<label id="sex-lbl">Giới tính<span class="required-label">*</span></label><br/>
							<select class="selectpicker" title="Vui lòng chọn" id="sex" name="sex">
									<option value="other" ${not empty stf && stf.sex == 'other' ? 'selected' : '' }>Khác</option>
									<option value="female" data-icon="fa fa-female" ${not empty stf && stf.sex == 'female' ? 'selected': '' }>Nữ</option>
									<option value="male" data-icon="fa fa-male" ${not empty stf && stf.sex == 'male' ? 'selected': '' }>Nam</option>
							</select>
						</div>
						<div class="form-group">
							<label id="devtest-lbl">Dev/Test<span class="required-label">*</span></label><br/>
							<select class="selectpicker" title="Vui lòng chọn" id="devtest" name="devtest">
								<option value="dev" ${not empty stf && stf.devtest == 'dev' ? 'selected': '' }>Dev</option>
								<option value="test" ${not empty stf && stf.devtest == 'test' ? 'selected': '' }>Test</option>
							</select>
						</div>
						<div class="form-group">
							<label id="school-lbl">Trường<span class="required-label">*</span></label><br/>
							<select class="selectpicker" title="Vui lòng chọn" data-live-search="true" id="school" name="university">
								<c:forEach items="${universityLst}" var="uni">
									<option value="${uni }" ${not empty stf && stf.university == uni ? 'selected': '' }>${uni }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label id="specialize-lbl">Ngành<span class="required-label">*</span></label>
							
							<c:if test="${not empty stf}">
								<input type="text" class="form-control" id="specialize" name="specialize" value="${stf.specialize }">
							</c:if>
							<c:if test="${empty stf}">
								<input type="text" class="form-control" id="specialize" name="specialize">
							</c:if>
						</div>
						<div class="form-group">
							<label id="learningCenter-lbl">Trung tâm học thêm</label>
							<c:if test="${not empty stf}">
								<input type="text" class="form-control" id="learningCenter" name="learningCenter" value="${stf.learningCenter }">
							</c:if>
							<c:if test="${empty stf}">
								<input type="text" class="form-control" id="learningCenter" name="learningCenter">
							</c:if>
						</div>
						<div class="form-group">
							<label id="graduation-date-lbl">Ngày tốt nghiệp</label>
							<div class="datepicker date input-group p-0">
								<div class="input-group-append">
									<span class="input-group-text date-label"><i class="fa fa-calendar"></i></span>
								</div>
								<c:if test="${not empty stf}">
									<input type="text" class="form-control" id="graduation-date" name="graduationdate" value="${stf.graduationDate}">
								</c:if>
								<c:if test="${empty stf}">
									<input type="text" class="form-control" id="graduation-date" name="graduationdate">
								</c:if>
							</div>
						</div>
						<div class="form-group">
							<label id="job-lbl">Nghề nghiệp<span class="required-label">*</span></label><br/>
							<select class="selectpicker" title="Vui lòng chọn" id="job" name="job">
								<option value="other" ${not empty stf && stf.job == 'other' ? 'selected': '' }>Khác</option>
								<option value="Cử nhân" ${not empty stf && stf.job == 'Cử nhân' ? 'selected': '' }>Cử nhân</option>
								<option value="Kỹ sư" ${not empty stf && stf.job == 'Kỹ sư' ? 'selected': '' }>Kỹ sư</option>
								<option value="Lập trình viên" ${not empty stf && stf.job == 'Lập trình viên' ? 'selected': '' }>Lập trình viên</option>
							</select>
						</div>
						<div class="form-group">
							<label id="nationality-lbl">Quốc tịch<span class="required-label">*</span></label>
							<c:if test="${not empty stf}">
								<input type="text" class="form-control" id="nationality" name="nationality" value="${stf.nationality}">
							</c:if>
							<c:if test="${empty stf}">
								<input type="text" class="form-control" id="nationality" name="nationality">
							</c:if>
						</div>
						<div class="form-group">
							<label id="birthday-lbl">Ngày sinh<span class="required-label">*</span></label>
							<div class="datepicker date input-group p-0">
								<div class="input-group-append">
									<span class="input-group-text date-label"><i class="fa fa-calendar"></i></span>
								</div>
								<c:if test="${not empty stf}">
									<input type="text" class="form-control" id="birthday" name="birthday" value="${stf.birthday}">
								</c:if>
								<c:if test="${empty stf}">
									<input type="text" class="form-control" id="birthday" name="birthday">
								</c:if>
							</div>
						</div>
						<div class="form-group">
							<label id="placeOfBirth-lbl">Nơi sinh<span class="required-label">*</span></label><br/>
							<select class="selectpicker" title="Vui lòng chọn" data-live-search="true" id="placeOfBirth" name="placeofbirth">
								<c:forEach items="${provincialLst}" var="provin">
									<option value="${provin }" ${not empty stf && stf.placeOfBirth == provin ? 'selected' : '' }>${provin }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label id="level-lbl">Trình độ<span class="required-label">*</span></label><br/>
							<select class="selectpicker" title="Vui lòng chọn" id="level" name="level">
								<option value="other" ${not empty stf && stf.level == 'other' ? 'selected' : '' }>Khác</option>
								<option value="Đại học" ${not empty stf && stf.level == 'Đại học' ? 'selected' : '' }>Đại học</option>
								<option value="Cao đẳng" ${not empty stf && stf.level == 'Cao đẳng' ? 'selected' : '' }>Cao đẳng</option>
								<option value="Trung tâm" ${not empty stf && stf.level == 'Trung tâm' ? 'selected' : '' }>Trung tâm</option>
								<option value="Phổ thông trung học" ${not empty stf && stf.level == 'Phổ thông trung học' ? 'selected' : '' }>Phổ thông trung học</option>
							</select>
						</div>
						<div class="form-group">
							<label id="address-lbl">Địa chỉ thường trú<span class="required-label">*</span></label>
							<c:if test="${not empty stf}">
								<input type="text" class="form-control" id="address" name="address" value="${stf.address }">
							</c:if>
							<c:if test="${empty stf}">
								<input type="text" class="form-control" id="address" name="address">
							</c:if>
						</div>
						<div class="form-group">
							<label id="cmnd-lbl">Số CMND<span class="required-label">*</span></label>
							<c:if test="${not empty stf}">
								<input type="text" class="form-control" id="cmnd" name="cmnd" value="${stf.cmnd }">
							</c:if>
							<c:if test="${empty stf}">
								<input type="text" class="form-control" id="cmnd" name="cmnd">
							</c:if>
						</div>
						<div class="form-group">
							<label id="cmnd-date-lbl">Ngày cấp CMND<span class="required-label">*</span></label>
							<div class="datepicker date input-group p-0">
								<div class="input-group-append">
									<span class="input-group-text date-label"><i class="fa fa-calendar"></i></span>
								</div>
								<c:if test="${not empty stf}">
									<input type="text" class="form-control" id="cmnd-date" name="cmnddate" value="${stf.cmndDate }">
								</c:if>
								<c:if test="${empty stf}">
									<input type="text" class="form-control" id="cmnd-date" name="cmnddate">
								</c:if>
							</div>
						</div>
						<div class="form-group">
							<label id="placeOfCMND-lbl">CMND cấp tại:<span class="required-label">*</span></label><br/>
							<select class="selectpicker" title="Vui lòng chọn" data-live-search="true" id="placeOfCMND" name="placeofcmnd">
								<c:forEach items="${provincialLst}" var="provin">
									<option value="CA. ${provin }" ${not empty stf && stf.placeOfCMND == 'CA. '.concat(provin) ? 'selected' : '' }>CA. ${provin }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label id="acc-lbl">Account<span class="required-label">*</span></label>
							<c:if test="${not empty stf}">
								<input type="text" class="form-control" id="acc" name="acc" value="${stf.acc }">
							</c:if>
							<c:if test="${empty stf}">
								<input type="text" class="form-control" id="acc" name="acc">
							</c:if>
						</div>
						<div class="form-group">
							<label id="mail-lbl">Mail<span class="required-label">*</span></label>
							<c:if test="${not empty stf}">
								<input type="text" class="form-control" id="mail" name="mail" value="${stf.mail }">
							</c:if>
							<c:if test="${empty stf}">
								<input type="text" class="form-control" id="mail" name="mail">
							</c:if>
						</div>
						<div class="form-group">
							<label id="phoneNumber-lbl">Số điện thoại<span class="required-label">*</span></label>
							<c:if test="${not empty stf}">
								<input type="text" class="form-control" id="phoneNumber" name="phoneNumber" value="${stf.phoneNumber }">
							</c:if>
							<c:if test="${empty stf}">
								<input type="text" class="form-control" id="phoneNumber" name="phoneNumber">
							</c:if>
						</div>
						<button id="btn-submit" type="button" class="btn btn-primary">Đăng kí</button>
					</form>
				</div>
			</div>
		</div>
		<script src="bootstrap/ajax/jquery/jquery.min.js"></script>
		<script src="bootstrap/ajax/popper/popper.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="bootstrap/js/bootstrap-datepicker.min.js"></script>
		<script src="bootstrap/js/bootstrap-select.min.js"></script>
		<script src="bootstrap/js/i18n/defaults-vi_VN.min.js"></script>
		<script src="js/common.js"></script>
		<script src="js/detail.js"></script>
	</body>
</html>