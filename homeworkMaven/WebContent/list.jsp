<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Danh sách nhân viên</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.css">
		<link rel="stylesheet" href="bootstrap/css/bootstrap-select.min.css">
		<link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.21/css/jquery.dataTables.css">
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" type="text/css" href="css/list.css">
	</head>
	<body>
		<div class="container">
			<!-- Content here -->
			<%
		        if(session != null && session.getAttribute("username") != null)  {
		    %>  
			<div class="header">
				<div class="info-login text-primary font-weight-bold"><p><%=session.getAttribute("username") %></p></div>
				<a href="logout" type="button" class="btn btn-dark btn-sm"
				title="logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
			</div>
			<%
	        	}
			%>
			<div class="main">
				<c:if test="${(not empty message) && fn:length(message) > 0 }">
					<p class="error">${message }</p>
				</c:if>
				<div class="add-search">
					<a href="createEdit" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></a>
					<button type="button" class="btn btn-success" id="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
					<button type="button" class="btn btn-warning" id="btn-export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
				</div>
				<form action="ExportServlet" method="post">
				<div class="area-export">
					<div class="input-group mb-3">
						<select class="selectpicker" title="Điều kiện xuất file">
							<option value="exp-class">Theo lớp</option>
							<option value="exp-createdDate">Theo ngày nhập</option>
						</select>
						<button type="submit" class="btn btn-warning btn-sm ml-3" id="btn-export-main">Xuất file<i class="fa fa-download pl-2 pr-2" aria-hidden="true"></i></button>
					</div>
					<div class="row area-exp-class">
						<div class="col-12 col-md-6 col-lg-3">
							<div class="input-group mb-3">
								<div class="input-group-prepend text-center">
									<span class="input-group-text" id="basic-addon1">Lớp</span>
								</div>
								<input type="text" class="form-control" name="expClazz" id="expClazz">
							</div>
						</div>
					</div>
					<div class="row area-exp-date">
						<div class="col-12 col-md-6 col-lg-3">
							<div class="input-group mb-3">
								<div class="datepicker date input-group p-0">
									<div class="input-group-append">
										<span class="input-group-text date-label"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" placeholder="(Ngày nhập) từ ngày" class="form-control" id="expDateFrom" name="expDateFrom">
								</div>
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-3">
							<div class="input-group mb-3">
								<div class="datepicker date input-group p-0">
									<div class="input-group-append">
										<span class="input-group-text date-label"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" placeholder="(Ngày nhập) đến ngày" class="form-control" id="expDateTo" name="expDateTo">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
				<div class="area-search">
					<div class="row">
						<div class="col-12 col-md-6 col-lg-3">
							<div class="input-group mb-3">
								<div class="input-group-prepend text-center">
									<span class="input-group-text" id="basic-addon1">Lớp</span>
								</div>
								<input type="text" class="form-control" name="searchClazz" id="searchClazz">
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-3">
							<div class="input-group mb-3">
								<div class="input-group-prepend text-center">
									<span class="input-group-text" id="basic-addon1">Account</span>
								</div>
								<input type="text" class="form-control" name="searchAcc" id="searchAcc">
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-3">
							<div class="form-group mb-3">
								<div class="datepicker date input-group p-0">
									<div class="input-group-append">
										<span class="input-group-text date-label"><i class="fa fa-calendar text-center"></i></span>
									</div>
									<input type="text" placeholder="(Ngày nhập) từ ngày" class="form-control" id="searchDateFrom" name="searchDateFrom">
								</div>
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-3">
							<div class="form-group mb-3">
								<div class="datepicker date input-group p-0">
									<div class="input-group-append">
										<span class="input-group-text date-label"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" placeholder="(Ngày nhập) đến ngày" class="form-control" id="searchDateTo" name="searchDateTo">
								</div>
							</div>
						</div>
					</div>
					<button type="button" class="btn btn-success dark btn-sm mb-3" id="btn-search-table">Tìm kiếm</button>
				</div>
				<div class="table-responsive">
					<table class="table table-hover table-bordered" id="tbStaff">
						<thead>
							<tr class="text-center">
								<th scope="col">#</th>
								<th scope="col">Lớp</th>
								<th scope="col">Tên</th>
								<th scope="col">Dev/test</th>
								<th scope="col">Ngày sinh</th>
								<th scope="col">Account</th>
								<th scope="col">Email</th>
								<th scope="col">Số điện thoại</th>
								<th scope="col">Ngày tạo</th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody id="table-body">
						<c:forEach items="${lst}" var="stf" varStatus="stt">
							<tr>
								<th scope="row"><c:out value="${stt.count }"></c:out> </th>
								<td>${stf.clazz }</td>
								<td>${stf.name }</td>
								<td>${stf.devtest }</td>
								<td>${stf.birthday }</td>
								<td>${stf.acc }</td>
								<td>${stf.mail }</td>
								<td>${stf.phoneNumber }</td>
								<td>${stf.createdDate }</td>
								<td>
									<a href="createEdit?id=${stf.id }" type="button" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
									<button type="button" class="btn btn-danger btn-sm" id="btn-del" onclick="del(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>
									<input type="hidden" value="${stf.id }"/>
								</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<script src="bootstrap/ajax/jquery/jquery.min.js"></script>
		<script src="bootstrap/ajax/popper/popper.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="bootstrap/js/bootstrap-datepicker.min.js"></script>
		<script src="bootstrap/js/bootstrap-select.min.js"></script>
		<script src="bootstrap/js/i18n/defaults-vi_VN.min.js"></script>
		<script type="text/javascript" charset="utf8" src="DataTables/DataTables-1.10.21/js/jquery.dataTables.min.js"></script>
		<script src="js/common.js"></script>
		<script src="js/list.js"></script>
	</body>
</html>