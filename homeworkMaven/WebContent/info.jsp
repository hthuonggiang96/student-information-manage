<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Thông tin nhân viên</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" type="text/css" href="css/info.css">
	</head>
	<body>
		<div class="container">
			<!-- Content here -->
			<%
		        if(session != null && session.getAttribute("username") != null)  {
		    %>  
			<div class="header">
				<div class="info-login text-primary font-weight-bold"><p><%=session.getAttribute("username") %></p></div>
				<a href="logout" type="button" class="btn btn-dark btn-sm"
				title="logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
			</div>
			<%
	        	}
			%>
			<c:if test="${(not empty message) && fn:length(message) > 0 }">
				<p class="error">${message }</p>
			</c:if>
			<c:if test="${not empty stf }">
				<table class="table table-bordered">
					<thead>
						<tr>
							<td colspan="6" class="text-info">Thông tin cá nhân vừa nhập</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">Lớp</th>
							<td colspan="5">${stf.clazz }</td>
						</tr>
						<tr>
							<th scope="row">Mã nhân viên</th>
							<td>${stf.code }</td>
							<th scope="row">Họ và tên</th>
							<td>${stf.name }</td>
							<th scope="row">Account</th>
							<td>${stf.acc }</td>
						</tr>
						<tr>
							<th scope="row">Giới tính</th>
							<td colspan="5">${stf.sex }</td>
						</tr>
						<tr>
							<th scope="row">Dev/Test</th>
							<td colspan="5">${stf.devtest }</td>
						</tr>
						<tr>
							<th scope="row">Năm (dự tính) tốt nghiệp</th>
							<td colspan="5">${stf.graduationYear }</td>
						</tr>
						<tr>
							<th scope="row">Trường</th>
							<td colspan="2">${stf.university }</td>
							<th scope="row">Ngành</th>
							<td colspan="2">${stf.specialize }</td>
						</tr>
						<tr>
							<th scope="row">Trung tâm học thêm</th>
							<td colspan="5">${stf.learningCenter }</td>
						</tr>
						<tr>
							<th scope="row">Ngày tốt nghiệp</th>
							<td colspan="5">${stf.graduationDate }</td>
						</tr>
						<tr>
							<th scope="row">Trình độ</th>
							<td colspan="5">${stf.level }</td>
						</tr>
						<tr>
							<th scope="row">Nghề nghiệp</th>
							<td colspan="5">${stf.job }</td>
						</tr>
						<tr>
							<th scope="row">Quốc tịch</th>
							<td colspan="5">${stf.nationality }</td>
						</tr>
						<tr>
							<th scope="row">Ngày sinh</th>
							<td colspan="5">${stf.birthday }</td>
						</tr>
						<tr>
							<th scope="row">Nơi sinh</th>
							<td colspan="5">${stf.placeOfBirth }</td>
						</tr>
						<tr>
							<th scope="row">Địa chỉ thường trú</th>
							<td colspan="5">${stf.address }</td>
						</tr>
						<tr>
							<th scope="row">Số CMND</th>
							<td colspan="5">${stf.cmnd }</td>
						</tr>
						<tr>
							<th scope="row">Ngày cấp</th>
							<td colspan="2">${stf.cmndDate }</td>
							<th scope="row">Cấp tại</th>
							<td colspan="2">${stf.placeOfCMND }</td>
						</tr>
						<tr>
							<th scope="row">Mail</th>
							<td colspan="5">${stf.mail }</td>
						</tr>
						<tr>
							<th scope="row">Số điện thoại</th>
							<td colspan="5">${stf.phoneNumber }</td>
						</tr>
					</tbody>
				</table>
			</c:if>
			
		</div>
		<script src="bootstrap/ajax/jquery/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="js/common.js"></script>
	</body>
</html>