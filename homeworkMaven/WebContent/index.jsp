<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Chọn quyền</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
	<body>
		<div class="container-fluid container-more">
			<div class="role">
				<div class="image">
					<img src="images/icons8-user-group-100.png" class="img-fluid">
				</div>
				<a class="area-label" href="createEdit">
					<div class="label"><h4 class="text-uppercase">guest</h4></div>
				</a>
			</div>
			<div class="line">
				</div>
			<div class="role">
				<div class="image">
					<img src="images/icons8-system-administrator-female-100.png" class="img-fluid">
				</div>
				<a class="area-label" href="login.jsp">
					<div class="label"><h4 class="text-uppercase">admin</h4></div>
				</a>
			</div>			
		</div>
		<script src="bootstrap/ajax/jquery/jquery.min.js"></script>
		<script src="bootstrap/ajax/popper/popper.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>