package com.giang.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.giang.model.bean.ResponeCommon;
import com.giang.model.bean.Staff;
import com.giang.model.bo.StaffBO;

/**
 * Servlet implementation class DetailServlet
 */
@WebServlet("/CreateServlet")
public class DetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		StaffBO bo = new StaffBO();
		
		HttpSession session = request.getSession(false);
		String id = request.getParameter("id");
		if (session != null && session.getAttribute("username") != null && id != null && !id.isEmpty()) {
			ResponeCommon rsStaff = bo.getInfoById(id);
			if (rsStaff.getMessage() != null && rsStaff.getMessage().length() > 0) {			
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/detail.jsp");
				request.setAttribute("message", rsStaff.getMessage().toString());
				rd.forward(request, response);
				return;
			}
			request.setAttribute("stf", rsStaff.getStf());
		}
		ResponeCommon rsUniversity = null;
		rsUniversity = bo.showUniversity();
		
		if (rsUniversity.getMessage() != null && rsUniversity.getMessage().length() > 0) {			
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/detail.jsp");
			request.setAttribute("message", rsUniversity.getMessage().toString());
			rd.forward(request, response);
			return;
		}
		
		ResponeCommon rsProvincial = null;
		rsProvincial = bo.showProvincial();
		
		if (rsProvincial.getMessage() != null && rsProvincial.getMessage().length() > 0) {			
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/detail.jsp");
			request.setAttribute("message", rsProvincial.getMessage().toString());
			rd.forward(request, response);
			return;
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/detail.jsp");
		request.setAttribute("universityLst", rsUniversity.getLstInfo());
		request.setAttribute("provincialLst", rsProvincial.getLstInfo());
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    response.setContentType("text/html; charset=UTF-8");
	    
		String id = request.getParameter("id");
		String clazz = request.getParameter("class");
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		String graduationYear = request.getParameter("graduationyear");
		String sex = request.getParameter("sex");
		String devtest = request.getParameter("devtest");
		String university = request.getParameter("university");
		String specialize = request.getParameter("specialize");
		String learningCenter = request.getParameter("learningCenter");
		String graduationDate = request.getParameter("graduationdate");
		String job = request.getParameter("job");
		String nationality = request.getParameter("nationality");
		String birthday = request.getParameter("birthday");
		String placeOfBirth = request.getParameter("placeofbirth");
		String level = request.getParameter("level");
		String address = request.getParameter("address");
		String cmnd = request.getParameter("cmnd");
		String cmndDate = request.getParameter("cmnddate");
		String placeOfCMND = request.getParameter("placeofcmnd");
		String acc = request.getParameter("acc");
		String mail = request.getParameter("mail");
		String phoneNumber = request.getParameter("phoneNumber");

		Staff stf = new Staff(clazz, code, name, graduationYear, sex, devtest, university, specialize, learningCenter,
				graduationDate, job, nationality, birthday, placeOfBirth, level, address, cmnd, cmndDate, placeOfCMND,
				acc, mail, phoneNumber);
		StaffBO bo = new StaffBO();
		ResponeCommon rs = null;
		if (id == null || id.isEmpty()) {
			rs = bo.create(stf);
		}else {
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("username") == null) {
				String message = "Vui lòng đăng nhập lại";
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
				request.setAttribute("message", message);
				rd.forward(request, response);
				return;
			}
			stf.setId(id);
			rs = bo.update(stf);
		}
		if (rs.getMessage() != null && rs.getMessage().length() > 0) {			
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/detail.jsp");
			request.setAttribute("message", rs.getMessage().toString());
			rd.forward(request, response);
			return;
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/info.jsp");
		request.setAttribute("stf", stf);
		rd.forward(request, response);
	}

}
