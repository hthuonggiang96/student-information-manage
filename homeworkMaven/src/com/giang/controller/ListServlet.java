package com.giang.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.giang.model.bean.ResponeCommon;
import com.giang.model.bean.Staff;
import com.giang.model.bo.StaffBO;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("username") == null) {
			String message = "Vui lòng đăng nhập lại";
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
			request.setAttribute("message", message);
			rd.forward(request, response);
			return;
		}
		String message = request.getParameter("message");

		Map<String, String> condition = new HashMap<String, String>();
		condition.put("searchClazz", request.getParameter("searchClazz"));
		condition.put("searchAcc", request.getParameter("searchAcc"));
		condition.put("searchDateFrom", request.getParameter("searchDateFrom"));
		condition.put("searchDateTo", request.getParameter("searchDateTo"));

		StaffBO bo = new StaffBO();
		ResponeCommon rs = null;
		rs = bo.showStaffWithCondition(condition);
		RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/list.jsp");
		if (message != null && message.length() > 0) {
			request.setAttribute("message", message);
		}
		request.setAttribute("lst", rs.getLst());
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("username") == null) {
			String message = "Vui lòng đăng nhập lại";
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
			request.setAttribute("message", message);
			rd.forward(request, response);
			return;
		}
		Map<String, String> condition = new HashMap<String, String>();
		condition.put("searchClazz", request.getParameter("searchClazz"));
		condition.put("searchAcc", request.getParameter("searchAcc"));
		condition.put("searchDateFrom", request.getParameter("searchDateFrom"));
		condition.put("searchDateTo", request.getParameter("searchDateTo"));

		StaffBO bo = new StaffBO();
		ResponeCommon rs = null;
		rs = bo.showStaffWithCondition(condition);
		StringBuilder tableBody = new StringBuilder();
		int stt = 0;
		for (Staff stf : rs.getLst()) {
			tableBody.append("<tr>");
			tableBody.append("<th scope='row'>"+(++stt)+"</th>"); //<input type="hidden" value="${stf.id }" id='staffID'/>
			tableBody.append("<td>"+stf.getClazz()+"</td>");
			tableBody.append("<td>"+stf.getName()+"</td>");
			tableBody.append("<td>"+stf.getDevtest()+"</td>");
			tableBody.append("<td>"+stf.getBirthday()+"</td>");
			tableBody.append("<td>"+stf.getAcc()+"</td>");
			tableBody.append("<td>"+stf.getMail()+"</td>");
			tableBody.append("<td>"+stf.getPhoneNumber()+"</td>");
			tableBody.append("<td>"+stf.getCreatedDate()+"</td>");
			tableBody.append("<td>");
			tableBody.append("<a href='createEdit?id="+stf.getId()+"' type='button' class='btn btn-info btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>");
			tableBody.append("<button type='button' class='btn btn-danger btn-sm' id='btn-del' onclick='del(this)'><i class='fa fa-trash' aria-hidden='true'></i></button>");
			tableBody.append("<input type='hidden' value='"+stf.getId()+"' />");
			tableBody.append("</td>");
			tableBody.append("</tr>");
		}
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain; charset=UTF-8");
		response.getWriter().write(tableBody.toString());
	}

}
