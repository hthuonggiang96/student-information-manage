package com.giang.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.giang.model.bean.ResponeCommon;
import com.giang.model.bo.StaffBO;

/**
 * Servlet implementation class DelServlet
 */
@WebServlet("/DelServlet")
public class DelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("username") == null) {
			String message = "Vui lòng đăng nhập lại";
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
			request.setAttribute("message", message);
			rd.forward(request, response);
			return;
		}
		String id = request.getParameter("id");
		StaffBO bo = new StaffBO();
		ResponeCommon rs = null;
		rs = bo.delById(id);
		if (rs.getMessage() != null && rs.getMessage().length() > 0) {			
			RequestDispatcher rd = request.getRequestDispatcher("ListServlet");
			request.setAttribute("message", rs.getMessage().toString());
			rd.forward(request, response);
			return;
		}
		RequestDispatcher rd = request.getRequestDispatcher("ListServlet");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
