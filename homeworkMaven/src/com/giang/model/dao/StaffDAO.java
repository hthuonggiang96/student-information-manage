package com.giang.model.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.giang.model.bean.Staff;
import com.mysql.jdbc.PreparedStatement;

public class StaffDAO {
	private ConnectionConfig con = new ConnectionConfig();
	private Connection conn = null;
	private PreparedStatement stmt = null;
	private ResultSet rs = null;

	public StaffDAO() {
	}

	private void closeAll() {
		try {
			if (conn != null) {
				conn.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public boolean createNoCheck(Staff stf) {
		boolean checkSuccess = false;
		String sql = "select id, clazz,code,name,graduationYear,sex,devtest,university,specialize,learningCenter,graduationDate,job,nationality,birthday,placeOfBirth,level,address,cmnd,cmndDate,placeOfCMND,acc,mail,phoneNumber,createdDate from staff";
		conn = con.getConnection();
		try {
			stmt = (PreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt.executeQuery();

			rs.moveToInsertRow();
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			
			rs.updateString("clazz", stf.getClazz());
			if (stf.getCode() != null && !stf.getCode().isEmpty()) {
				rs.updateString("code", stf.getCode());
			}
			rs.updateString("name", stf.getName());
			rs.updateString("graduationyear", stf.getGraduationYear());
			rs.updateString("sex", stf.getSex());
			rs.updateString("devtest", stf.getDevtest());
			rs.updateString("university", stf.getUniversity());
			rs.updateString("specialize", stf.getSpecialize());
			if (stf.getLearningCenter() != null && !stf.getLearningCenter().isEmpty()) {
				rs.updateString("learningcenter", stf.getLearningCenter());
			}
			if (stf.getGraduationDate() != null && !stf.getGraduationDate().isEmpty()) {
				LocalDate localGraduationDate = LocalDate.parse(stf.getGraduationDate(), formatter);
				rs.updateDate("graduationdate", Date.valueOf(localGraduationDate));
			}
			rs.updateString("job", stf.getJob());
			rs.updateString("nationality", stf.getNationality());
			
			LocalDate localBirthday = LocalDate.parse(stf.getBirthday(), formatter);
			rs.updateDate("birthday", Date.valueOf(localBirthday));
			
			rs.updateString("placeofbirth", stf.getPlaceOfBirth());
			rs.updateString("level", stf.getLevel());
			rs.updateString("address", stf.getAddress());
			rs.updateString("cmnd", stf.getCmnd());
			
			LocalDate localCMNDDate = LocalDate.parse(stf.getCmndDate(), formatter);
			rs.updateDate("cmnddate", Date.valueOf(localCMNDDate));
			
			rs.updateString("placeofcmnd", stf.getPlaceOfCMND());
			rs.updateString("acc", stf.getAcc());
			rs.updateString("mail", stf.getMail());
			rs.updateString("phonenumber", stf.getPhoneNumber());
			
			LocalDate currentDate = LocalDate.now();
			rs.updateDate("createddate", Date.valueOf(currentDate));
			
			rs.insertRow();
			checkSuccess = true;
		} catch (SQLException e) {
			e.printStackTrace();
			checkSuccess = false;
		}finally {
			closeAll();
		}
		return checkSuccess;
	}

	public List<String> showUniversity() {
		String sql = "select name from university";
		List<String> lst = new ArrayList<>();
		try {
			conn = con.getConnection();
			stmt = (PreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery();
			while (rs.next()) {
				lst.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			closeAll();
		}
		return lst;
	}

	public List<String> showProvincial() {
		String sql = "select name from provincial";
		List<String> lst = new ArrayList<>();
		try {
			conn = con.getConnection();
			stmt = (PreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery();
			while (rs.next()) {
				lst.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			closeAll();
		}
		return lst;
	}

	public boolean isExistAdmin(String username, String password) {
		String sql = "select username, password from adminn where delflg <> 1 and username = ? and password = ?";
		boolean checkExist = false;
		try {
			conn = con.getConnection();
			stmt = (PreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stmt.setString(1, username);
			stmt.setString(2, password);
			rs = stmt.executeQuery();
			while (rs.next()) {
				checkExist  = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			closeAll();
		}
		return checkExist;
	}

	public List<Staff> showAllStaff() {
		String sql = "select id, clazz,name,devtest,birthday,acc,mail,phoneNumber from staff where delflg <> 1";
		List<Staff> lst = new ArrayList<>();
		try {
			conn = con.getConnection();
			stmt = (PreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery();
			while (rs.next()) {
				Staff stf = new Staff();
				stf.setId(rs.getString("id"));
				stf.setClazz(rs.getString("clazz"));
				stf.setName(rs.getString("name"));
				stf.setDevtest(rs.getString("devtest"));
				
				Date datesql = rs.getDate("birthday");
				LocalDate localdate = datesql.toLocalDate();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				stf.setBirthday(formatter.format(localdate));
				
				stf.setAcc(rs.getString("acc"));
				stf.setMail(rs.getString("mail"));
				stf.setPhoneNumber(rs.getString("phoneNumber"));
				lst.add(stf);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			closeAll();
		}
		return lst;
	}

	public Staff getInfoById(String id) {
		String sql = "select id,clazz,code,name,graduationYear,sex,devtest,university,specialize,learningCenter,graduationDate,job,nationality,birthday,placeOfBirth,level,address,cmnd,cmndDate,placeOfCMND,acc,mail,phoneNumber from staff where delflg <> 1 and id = ?";
		Staff stf = null;
		try {
			conn = con.getConnection();
			stmt = (PreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stmt.setString(1, id);
			rs = stmt.executeQuery();
			while (rs.next()) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				
				stf = new Staff();
				
				stf.setId(rs.getString("id"));
				stf.setClazz(rs.getString("clazz"));
				stf.setCode(rs.getString("code"));
				stf.setName(rs.getString("name"));
				stf.setGraduationYear(rs.getString("graduationYear"));
				stf.setSex(rs.getString("sex"));
				stf.setDevtest(rs.getString("devtest"));
				stf.setUniversity(rs.getString("university"));
				stf.setSpecialize(rs.getString("specialize"));
				stf.setLearningCenter(rs.getString("learningCenter"));
				
				if (rs.getDate("graduationDate") != null) {
					Date datesqlGraduationDate = rs.getDate("graduationDate");
					LocalDate ldGraduationDate = datesqlGraduationDate.toLocalDate();
					stf.setGraduationDate(formatter.format(ldGraduationDate));
				}
				
				stf.setJob(rs.getString("job"));
				stf.setNationality(rs.getString("nationality"));
				
				Date datesqlBirthday = rs.getDate("birthday");
				LocalDate localdateBirthday = datesqlBirthday.toLocalDate();
				stf.setBirthday(formatter.format(localdateBirthday));
				
				stf.setPlaceOfBirth(rs.getString("placeOfBirth"));
				stf.setLevel(rs.getString("level"));
				stf.setAddress(rs.getString("address"));
				stf.setCmnd(rs.getString("cmnd"));
				
				Date datesqlCmndDate = rs.getDate("cmndDate");
				LocalDate ldCmndDate = datesqlCmndDate.toLocalDate();
				stf.setCmndDate(formatter.format(ldCmndDate));
				
				stf.setPlaceOfCMND(rs.getString("placeOfCMND"));
				stf.setAcc(rs.getString("acc"));
				stf.setMail(rs.getString("mail"));
				stf.setPhoneNumber(rs.getString("phoneNumber"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			closeAll();
		}
		return stf;
	}

	public boolean update(Staff stf) {
		boolean checkSuccess = false;
		String sql = "select id, clazz,code,name,graduationYear,sex,devtest,university,specialize,learningCenter,graduationDate,job,nationality,birthday,placeOfBirth,level,address,cmnd,cmndDate,placeOfCMND,acc,mail,phoneNumber,createdDate from staff where id = ? and delflg <> 1";
		conn = con.getConnection();
		try {
			stmt = (PreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			stmt.setString(1, stf.getId());
			rs = stmt.executeQuery();
			while (rs.next()) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				
				rs.updateString("clazz", stf.getClazz());
				if (stf.getCode() != null && !stf.getCode().isEmpty()) {
					rs.updateString("code", stf.getCode());
				}
				rs.updateString("name", stf.getName());
				rs.updateString("graduationyear", stf.getGraduationYear());
				rs.updateString("sex", stf.getSex());
				rs.updateString("devtest", stf.getDevtest());
				rs.updateString("university", stf.getUniversity());
				rs.updateString("specialize", stf.getSpecialize());
				if (stf.getLearningCenter() != null && !stf.getLearningCenter().isEmpty()) {
					rs.updateString("learningcenter", stf.getLearningCenter());
				}
				if (stf.getGraduationDate() != null && !stf.getGraduationDate().isEmpty()) {
					LocalDate localGraduationDate = LocalDate.parse(stf.getGraduationDate(), formatter);
					rs.updateDate("graduationdate", Date.valueOf(localGraduationDate));
				}
				rs.updateString("job", stf.getJob());
				rs.updateString("nationality", stf.getNationality());
				
				LocalDate localBirthday = LocalDate.parse(stf.getBirthday(), formatter);
				rs.updateDate("birthday", Date.valueOf(localBirthday));
				
				rs.updateString("placeofbirth", stf.getPlaceOfBirth());
				rs.updateString("level", stf.getLevel());
				rs.updateString("address", stf.getAddress());
				rs.updateString("cmnd", stf.getCmnd());
				
				LocalDate localCMNDDate = LocalDate.parse(stf.getCmndDate(), formatter);
				rs.updateDate("cmnddate", Date.valueOf(localCMNDDate));
				
				rs.updateString("placeofcmnd", stf.getPlaceOfCMND());
				rs.updateString("acc", stf.getAcc());
				rs.updateString("mail", stf.getMail());
				rs.updateString("phonenumber", stf.getPhoneNumber());
				
				rs.updateRow();
				checkSuccess = true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			checkSuccess = false;
		}finally {
			closeAll();
		}
		return checkSuccess;
	}

	public int delById(String id) {
		int row = 0;
		String sql = "update staff set delflg = 1 where id = ? and delflg <> 1";
		conn = con.getConnection();
		try {
			stmt = (PreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			stmt.setString(1, id);
			row = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			closeAll();
		}
		return row;
	}

	public List<Staff> showStaffWithCondition(Map<String, String> condition) {
		String searchClazz = condition.get("searchClazz");
		String searchAcc = condition.get("searchAcc");
		String searchDateFrom = condition.get("searchDateFrom");
		String searchDateTo = condition.get("searchDateTo");
//		StringBuilder sql = new StringBuilder("select id,clazz,name,devtest,birthday,acc,mail,phoneNumber,createddate  from staff where delflg <> 1");
		StringBuilder sql = new StringBuilder("select id,clazz,code,name,graduationYear,sex,devtest,university,specialize,learningCenter,graduationDate,job,nationality,birthday,placeOfBirth,level,address,cmnd,cmndDate,placeOfCMND,acc,mail,phoneNumber,createddate  from staff where delflg <> 1");
		
		if (searchClazz != null && !searchClazz.trim().isEmpty()) {
			sql.append(" and clazz = '"+searchClazz.trim()+"'");
		}
		if (searchAcc != null && !searchAcc.trim().isEmpty()) {
			sql.append(" and acc = '"+searchAcc.trim()+"'");
		}
		if (searchDateFrom != null && !searchDateFrom.trim().isEmpty()) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate localDateFrom = LocalDate.parse(searchDateFrom.trim(), formatter);
			sql.append(" and DATE(createddate) >= '"+localDateFrom+"'");
		}
		if (searchDateTo != null && !searchDateTo.trim().isEmpty()) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate localDateTo = LocalDate.parse(searchDateTo.trim(), formatter);
			sql.append(" and DATE(createddate) <= '"+localDateTo+"'");
		}
		List<Staff> lst = new ArrayList<>();
		System.out.println(sql.toString());
		try {
			conn = con.getConnection();
			stmt = (PreparedStatement) conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery();
			while (rs.next()) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				
				Staff stf = new Staff();
				stf.setId(rs.getString("id"));
				stf.setClazz(rs.getString("clazz"));
				stf.setCode(rs.getString("code"));
				stf.setName(rs.getString("name"));
				stf.setGraduationYear(rs.getString("graduationYear"));
				stf.setSex(rs.getString("sex"));
				stf.setDevtest(rs.getString("devtest"));
				stf.setUniversity(rs.getString("university"));
				stf.setSpecialize(rs.getString("specialize"));
				stf.setLearningCenter(rs.getString("learningCenter"));
				
				if (rs.getDate("graduationDate") != null) {
					Date graduationDateSql = rs.getDate("graduationDate");
					LocalDate ldGraduationDate = graduationDateSql.toLocalDate();
					stf.setGraduationDate(formatter.format(ldGraduationDate));
				}
				
				stf.setJob(rs.getString("job"));
				stf.setNationality(rs.getString("nationality"));
				
				Date datesql = rs.getDate("birthday");
				LocalDate localdate = datesql.toLocalDate();
				stf.setBirthday(formatter.format(localdate));
				
				stf.setPlaceOfBirth(rs.getString("placeOfBirth"));
				stf.setLevel(rs.getString("level"));
				stf.setAddress(rs.getString("address"));
				stf.setCmnd(rs.getString("cmnd"));
				
				Date cmndDateSql = rs.getDate("cmndDate");
				LocalDate ldCmndDate = cmndDateSql.toLocalDate();
				stf.setCmndDate(formatter.format(ldCmndDate));
				
				stf.setPlaceOfCMND(rs.getString("placeOfCMND"));
				stf.setAcc(rs.getString("acc"));
				stf.setMail(rs.getString("mail"));
				stf.setPhoneNumber(rs.getString("phoneNumber"));
				
				Date createddatesql = rs.getDate("createddate");
				LocalDate lccreateddate = createddatesql.toLocalDate();
				stf.setCreatedDate(formatter.format(lccreateddate));
				lst.add(stf);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			closeAll();
		}
		return lst;
	}

}
