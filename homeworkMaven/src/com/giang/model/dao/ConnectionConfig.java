package com.giang.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionConfig {
	public Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String connectionURL = "jdbc:mysql://localhost:3306/staffproject?useSSL=false";
			conn = (Connection) DriverManager.getConnection(connectionURL, "root","123456");
			System.out.println("connect success");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			System.out.println("connect fail");
		}
		return conn;
	}
	public void closeConnection(Connection conn) {
		try {
			conn.close();
			System.out.println("close connect success");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("close connect fail");
		}
	}
}
