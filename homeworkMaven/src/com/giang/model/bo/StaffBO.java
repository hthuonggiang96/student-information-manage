package com.giang.model.bo;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.giang.model.bean.ResponeCommon;
import com.giang.model.bean.Staff;
import com.giang.model.dao.StaffDAO;

public class StaffBO {
	public ResponeCommon create(Staff stf) {
		StringBuilder message = new StringBuilder();
		//check validate - begin
		//check validate - end
		StaffDAO dao = new StaffDAO();
		if (dao.createNoCheck(stf)) {
			return new ResponeCommon(null, null, null);
		}
		message.append("Tạo mới nhân viên thất bại");
		return new ResponeCommon(message, null, null);
	}

	public ResponeCommon showUniversity() {
		StaffDAO dao = new StaffDAO();
		List<String> lst = dao.showUniversity();
		ResponeCommon rs = new ResponeCommon();
		if (lst == null || lst.size() == 0) {
			rs.setMessage(new StringBuilder("Danh sách trường đại học bị rỗng"));
		}
		rs.setLstInfo(lst);
		return rs;
	}

	public ResponeCommon showProvincial() {
		StaffDAO dao = new StaffDAO();
		List<String> lst = dao.showProvincial();
		ResponeCommon rs = new ResponeCommon();
		if (lst == null || lst.size() == 0) {
			rs.setMessage(new StringBuilder("Danh sách tỉnh thành bị rỗng"));
		}
		rs.setLstInfo(lst);
		return rs;
	}

	public ResponeCommon login(String username, String password) {
		StringBuilder message = new StringBuilder();
		if (username == null || username.isEmpty()) {
			message.append("Username không được để trống");
		}
		if (password == null || password.isEmpty()) {
			message.append("Password không được để trống");
		}
		
		StaffDAO dao = new StaffDAO();
		ResponeCommon rs = new ResponeCommon();
		if (!dao.isExistAdmin(username, password)) {
			message.append("Accoutn này không tồn tại!!!");
			rs.setMessage(message);
		}
		return rs;
	}

	public ResponeCommon showAllStaff() {
		StaffDAO dao = new StaffDAO();
		List<Staff> lst = dao.showAllStaff();
		ResponeCommon rs = new ResponeCommon();
		if (lst == null || lst.size() == 0) {
			rs.setMessage(new StringBuilder("Danh sách Nhân viên bị rỗng"));
		}
		rs.setLst(lst);
		return rs;
	}

	public ResponeCommon getInfoById(String id) {
		StaffDAO dao = new StaffDAO();
		Staff stf = dao.getInfoById(id);
		ResponeCommon rs = new ResponeCommon();
		if (stf == null) {
			rs.setMessage(new StringBuilder("Có thể nhân viên đã bị xóa"));
		}
		rs.setStf(stf);
		return rs;
	}

	public ResponeCommon update(Staff stf) {
		StringBuilder message = new StringBuilder();
		//check validate - begin
		//check validate - end
		StaffDAO dao = new StaffDAO();
		if (dao.update(stf)) {
			return new ResponeCommon(null, null, null);
		}
		message.append("Chỉnh sửa thông tin nhân viên thất bại");
		return new ResponeCommon(message, null, null);
	}

	public ResponeCommon delById(String id) {
		StringBuilder message = new StringBuilder();
		StaffDAO dao = new StaffDAO();
		if (dao.delById(id) > 0) {
			return new ResponeCommon(null, null, null);
		}
		message.append("Có thể nhân viên này đã bị xóa trước đó");
		return new ResponeCommon(message, null, null);
	}

	public ResponeCommon showStaffWithCondition(Map<String, String> condition) {
		StaffDAO dao = new StaffDAO();
		List<Staff> lst = dao.showStaffWithCondition(condition);
		ResponeCommon rs = new ResponeCommon();
		if (lst == null || lst.size() == 0) {
			rs.setMessage(new StringBuilder("Danh sách Nhân viên bị rỗng"));
		}
		rs.setLst(lst);
		return rs;
	}

	public void exportFileExcel(Map<String, String> condition, OutputStream outputStream) {
		StaffDAO dao = new StaffDAO();
		List<Staff> lst = dao.showStaffWithCondition(condition);
//		ResponeCommon rs = new ResponeCommon();
//		if (lst == null || lst.size() == 0) {
//			rs.setMessage(new StringBuilder("Danh sách Nhân viên bị rỗng"));
//			return rs;
//		}
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet  sheet = workbook.createSheet("Danh sách nhân viên");
		
		int rowNum = 0;
		Row row;
		
		CellStyle style = createStyleForTitle(workbook);
		CellStyle styleData = createStyleForData(workbook);
		
		row = sheet.createRow(rowNum);
		row.setHeight((short) 1400); 
		
		getCellTitle(row, 0, "STT", style);
		getCellTitle(row, 1, "Lớp", style);
		getCellTitle(row, 2, "MNV", style);
		getCellTitle(row, 3, "Họ và Tên", style);
		getCellTitle(row, 4, "Năm Tốt nghiêp", style);
		getCellTitle(row, 5, "Giới tính", style);
		getCellTitle(row, 6, "Dev/Test", style);
		getCellTitle(row, 7, "Trường", style);
		getCellTitle(row, 8, "Nghành", style);
		getCellTitle(row, 9, "Trung tâm đã học thêm", style);
		getCellTitle(row, 10, "Ngày cấp", style);
		getCellTitle(row, 11, "Nghề nghiệp", style);
		getCellTitle(row, 12, "Quốc Tịch", style);
		getCellTitle(row, 13, "NTN/ sinh", style);
		getCellTitle(row, 14, "Ngày sinh", style);
		getCellTitle(row, 15, "tháng sinh", style);
		getCellTitle(row, 16, "năm sinh", style);
		getCellTitle(row, 17, "Nơi Sinh", style);
		getCellTitle(row, 18, "Trình Độ", style);
		getCellTitle(row, 19, "Địa chỉ thường trú", style);
		getCellTitle(row, 20, "Số CMND", style);
		getCellTitle(row, 21, "Ngày tháng năm cấp CMND", style);
		getCellTitle(row, 22, "Ngày cấp CMNN", style);
		getCellTitle(row, 23, "Tháng", style);
		getCellTitle(row, 24, "Năm cấp", style);
		getCellTitle(row, 25, "Tại", style);
		getCellTitle(row, 26, "ACC", style);
		getCellTitle(row, 27, "Mail", style);
		getCellTitle(row, 28, "Số điện thoại", style);
		
		//set Data
		for (Staff s : lst) {
			rowNum++;
			row = sheet.createRow(rowNum);
			
			getCellData(row, 0, String.valueOf(rowNum), styleData);
			getCellData(row, 1, s.getClazz(), styleData);
			getCellData(row, 2, s.getCode(), styleData);
			getCellData(row, 3, s.getName(), styleData);
			getCellData(row, 4, s.getGraduationYear(), styleData);
			getCellData(row, 5, s.getSex(), styleData);
			getCellData(row, 6, s.getDevtest(), styleData);
			getCellData(row, 7, s.getUniversity(), styleData);
			getCellData(row, 8, s.getSpecialize(), styleData);
			getCellData(row, 9, s.getLearningCenter(), styleData);
			getCellData(row, 10, s.getGraduationDate(), styleData);
			getCellData(row, 11, s.getJob(), styleData);
			getCellData(row, 12, s.getNationality(), styleData);
			getCellData(row, 13, s.getBirthday(), styleData);
			if (s.getBirthday() != null && !s.getBirthday().trim().isEmpty()) {
				getCellData(row, 14, (s.getBirthday().split("/"))[0], styleData);
				getCellData(row, 15, (s.getBirthday().split("/"))[1], styleData);
				getCellData(row, 16, (s.getBirthday().split("/"))[2], styleData);
			}else {
				getCellData(row, 14, "", styleData);
				getCellData(row, 15, "", styleData);
				getCellData(row, 16, "", styleData);
			}
			getCellData(row, 17, s.getPlaceOfBirth(), styleData);
			getCellData(row, 18, s.getLevel(), styleData);
			getCellData(row, 19, s.getAddress(), styleData);
			getCellData(row, 20, s.getCmnd(), styleData);
			getCellData(row, 21, s.getCmndDate(), styleData);
			if (s.getCmndDate() != null && !s.getCmndDate().trim().isEmpty()) {
				getCellData(row, 22, (s.getCmndDate().split("/"))[0], styleData);
				getCellData(row, 23, (s.getCmndDate().split("/"))[1], styleData);
				getCellData(row, 24, (s.getCmndDate().split("/"))[2], styleData);
			}else {
				getCellData(row, 22, "", styleData);
				getCellData(row, 23, "", styleData);
				getCellData(row, 24, "", styleData);
			}
			getCellData(row, 25, s.getPlaceOfCMND(), styleData);
			getCellData(row, 26, s.getAcc(), styleData);
			getCellData(row, 27, s.getMail(), styleData);
			getCellData(row, 28, s.getPhoneNumber(), styleData);
		}
		
		for (int i = 0; i <= 28; i++) {
			sheet.autoSizeColumn(i);
		}
		
//		File file = new File("D:/GiangHTH1/staff.xls");
//		file.getParentFile().mkdir();
		
/*		FileOutputStream outFile = null;
		try {
			outFile = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}*/
		try {
			workbook.write(outputStream);
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void createFile(List<Staff> lst, OutputStream outputStream) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet  sheet = workbook.createSheet("Danh sách nhân viên");
		
		int rowNum = 0;
		Row row;
		
		CellStyle style = createStyleForTitle(workbook);
		CellStyle styleData = createStyleForData(workbook);
		
		row = sheet.createRow(rowNum);
		row.setHeight((short) 1400); 
		
		getCellTitle(row, 0, "STT", style);
		getCellTitle(row, 1, "Lớp", style);
		getCellTitle(row, 2, "MNV", style);
		getCellTitle(row, 3, "Họ và Tên", style);
		getCellTitle(row, 4, "Năm Tốt nghiêp", style);
		getCellTitle(row, 5, "Giới tính", style);
		getCellTitle(row, 6, "Dev/Test", style);
		getCellTitle(row, 7, "Trường", style);
		getCellTitle(row, 8, "Nghành", style);
		getCellTitle(row, 9, "Trung tâm đã học thêm", style);
		getCellTitle(row, 10, "Ngày cấp", style);
		getCellTitle(row, 11, "Nghề nghiệp", style);
		getCellTitle(row, 12, "Quốc Tịch", style);
		getCellTitle(row, 13, "NTN/ sinh", style);
		getCellTitle(row, 14, "Ngày sinh", style);
		getCellTitle(row, 15, "tháng sinh", style);
		getCellTitle(row, 16, "năm sinh", style);
		getCellTitle(row, 17, "Nơi Sinh", style);
		getCellTitle(row, 18, "Trình Độ", style);
		getCellTitle(row, 19, "Địa chỉ thường trú", style);
		getCellTitle(row, 20, "Số CMND", style);
		getCellTitle(row, 21, "Ngày tháng năm cấp CMND", style);
		getCellTitle(row, 22, "Ngày cấp CMNN", style);
		getCellTitle(row, 23, "Tháng", style);
		getCellTitle(row, 24, "Năm cấp", style);
		getCellTitle(row, 25, "Tại", style);
		getCellTitle(row, 26, "ACC", style);
		getCellTitle(row, 27, "Mail", style);
		getCellTitle(row, 28, "Số điện thoại", style);
		
		//set Data
		for (Staff s : lst) {
			rowNum++;
			row = sheet.createRow(rowNum);
			
			getCellData(row, 0, String.valueOf(rowNum), styleData);
			getCellData(row, 1, s.getClazz(), styleData);
			getCellData(row, 2, s.getCode(), styleData);
			getCellData(row, 3, s.getName(), styleData);
			getCellData(row, 4, s.getGraduationYear(), styleData);
			getCellData(row, 5, s.getSex(), styleData);
			getCellData(row, 6, s.getDevtest(), styleData);
			getCellData(row, 7, s.getUniversity(), styleData);
			getCellData(row, 8, s.getSpecialize(), styleData);
			getCellData(row, 9, s.getLearningCenter(), styleData);
			getCellData(row, 10, s.getGraduationDate(), styleData);
			getCellData(row, 11, s.getJob(), styleData);
			getCellData(row, 12, s.getNationality(), styleData);
			getCellData(row, 13, s.getBirthday(), styleData);
			if (s.getBirthday() != null && !s.getBirthday().trim().isEmpty()) {
				getCellData(row, 14, (s.getBirthday().split("/"))[0], styleData);
				getCellData(row, 15, (s.getBirthday().split("/"))[1], styleData);
				getCellData(row, 16, (s.getBirthday().split("/"))[2], styleData);
			}else {
				getCellData(row, 14, "", styleData);
				getCellData(row, 15, "", styleData);
				getCellData(row, 16, "", styleData);
			}
			getCellData(row, 17, s.getPlaceOfBirth(), styleData);
			getCellData(row, 18, s.getLevel(), styleData);
			getCellData(row, 19, s.getAddress(), styleData);
			getCellData(row, 20, s.getCmnd(), styleData);
			getCellData(row, 21, s.getCmndDate(), styleData);
			if (s.getCmndDate() != null && !s.getCmndDate().trim().isEmpty()) {
				getCellData(row, 22, (s.getCmndDate().split("/"))[0], styleData);
				getCellData(row, 23, (s.getCmndDate().split("/"))[1], styleData);
				getCellData(row, 24, (s.getCmndDate().split("/"))[2], styleData);
			}else {
				getCellData(row, 22, "", styleData);
				getCellData(row, 23, "", styleData);
				getCellData(row, 24, "", styleData);
			}
			getCellData(row, 25, s.getPlaceOfCMND(), styleData);
			getCellData(row, 26, s.getAcc(), styleData);
			getCellData(row, 27, s.getMail(), styleData);
			getCellData(row, 28, s.getPhoneNumber(), styleData);
		}
		
		for (int i = 0; i <= 28; i++) {
			sheet.autoSizeColumn(i);
		}
		
//		File file = new File("D:/GiangHTH1/staff.xls");
//		file.getParentFile().mkdir();
		
/*		FileOutputStream outFile = null;
		try {
			outFile = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}*/
		try {
			workbook.write(outputStream);
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("Created file: " + file.getAbsolutePath());
	}
	
	private Cell getCellTitle(Row row, int position, String value, CellStyle style) {
		Cell cell;
		cell = row.createCell(position, CellType.STRING);
		cell.setCellValue(value);
		cell.setCellStyle(style);
		return cell;
	}
	
	private Cell getCellData(Row row, int position, String value, CellStyle style) {
		return getCellTitle(row, position, value, style);
	}

	private static CellStyle createStyleForTitle(XSSFWorkbook workbook) {
		Font font = workbook.createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short)11);
		font.setFontName("Times New Roman");
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.SEA_GREEN.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		
		style.setFont(font);
		return style;
	}
	
	private static CellStyle createStyleForData(XSSFWorkbook workbook) {
		Font font = workbook.createFont();
		font.setFontHeightInPoints((short)11);
		font.setFontName("Times New Roman");
		
		CellStyle style = workbook.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		
		style.setFont(font);
		return style;
	}
}
