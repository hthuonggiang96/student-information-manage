package com.giang.model.bean;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ResponeCommon {
	private StringBuilder message;
	private Staff stf;
	private List<Staff> lst;
	private List<String> lstInfo;
	private HSSFWorkbook wb;
	
	public ResponeCommon() {}
	
	public ResponeCommon(StringBuilder message, Staff stf, List<Staff> lst) {
		this.message = message;
		this.stf = stf;
		this.lst = lst;
	}

	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	public Staff getStf() {
		return stf;
	}
	public void setStf(Staff stf) {
		this.stf = stf;
	}
	public List<Staff> getLst() {
		return lst;
	}
	public void setLst(List<Staff> lst) {
		this.lst = lst;
	}

	public List<String> getLstInfo() {
		return lstInfo;
	}

	public void setLstInfo(List<String> lstInfo) {
		this.lstInfo = lstInfo;
	}

	public HSSFWorkbook getWb() {
		return wb;
	}

	public void setWb(HSSFWorkbook wb) {
		this.wb = wb;
	}
	
}
