package com.giang.model.bean;

public class Staff {
	private String id;
	private String clazz;
	private String code;
	private String name;
	private String graduationYear;
	private String sex;
	private String devtest;
	private String university;
	private String specialize;
	private String learningCenter;
	private String graduationDate;
	private String job;
	private String nationality;
	private String birthday;
	private String placeOfBirth;
	private String level;
	private String address;
	private String cmnd;
	private String cmndDate;
	private String placeOfCMND;
	private String acc;
	private String mail;
	private String phoneNumber;
	private String createdDate;
	private int delFlg;
	
	public Staff() {
	}

	public Staff(String clazz, String code, String name, String graduationYear, String sex, String devtest,
			String university, String specialize, String learningCenter, String graduationDate, String job,
			String nationality, String birthday, String placeOfBirth, String level, String address, String cmnd,
			String cmndDate, String placeOfCMND, String acc, String mail, String phoneNumber, String createdDate) {

		this.clazz = clazz;
		this.code = code;
		this.name = name;
		this.graduationYear = graduationYear;
		this.sex = sex;
		this.devtest = devtest;
		this.university = university;
		this.specialize = specialize;
		this.learningCenter = learningCenter;
		this.graduationDate = graduationDate;
		this.job = job;
		this.nationality = nationality;
		this.birthday = birthday;
		this.placeOfBirth = placeOfBirth;
		this.level = level;
		this.address = address;
		this.cmnd = cmnd;
		this.cmndDate = cmndDate;
		this.placeOfCMND = placeOfCMND;
		this.acc = acc;
		this.mail = mail;
		this.phoneNumber = phoneNumber;
		this.createdDate = createdDate;
	}
	
	public Staff(String clazz, String code, String name, String graduationYear, String sex, String devtest,
			String university, String specialize, String learningCenter, String graduationDate, String job,
			String nationality, String birthday, String placeOfBirth, String level, String address, String cmnd,
			String cmndDate, String placeOfCMND, String acc, String mail, String phoneNumber) {

		this.clazz = clazz;
		this.code = code;
		this.name = name;
		this.graduationYear = graduationYear;
		this.sex = sex;
		this.devtest = devtest;
		this.university = university;
		this.specialize = specialize;
		this.learningCenter = learningCenter;
		this.graduationDate = graduationDate;
		this.job = job;
		this.nationality = nationality;
		this.birthday = birthday;
		this.placeOfBirth = placeOfBirth;
		this.level = level;
		this.address = address;
		this.cmnd = cmnd;
		this.cmndDate = cmndDate;
		this.placeOfCMND = placeOfCMND;
		this.acc = acc;
		this.mail = mail;
		this.phoneNumber = phoneNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGraduationYear() {
		return graduationYear;
	}

	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getDevtest() {
		return devtest;
	}

	public void setDevtest(String devtest) {
		this.devtest = devtest;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public String getSpecialize() {
		return specialize;
	}

	public void setSpecialize(String specialize) {
		this.specialize = specialize;
	}

	public String getLearningCenter() {
		return learningCenter;
	}

	public void setLearningCenter(String learningCenter) {
		this.learningCenter = learningCenter;
	}

	public String getGraduationDate() {
		return graduationDate;
	}

	public void setGraduationDate(String graduationDate) {
		this.graduationDate = graduationDate;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCmnd() {
		return cmnd;
	}

	public void setCmnd(String cmnd) {
		this.cmnd = cmnd;
	}

	public String getCmndDate() {
		return cmndDate;
	}

	public void setCmndDate(String cmndDate) {
		this.cmndDate = cmndDate;
	}

	public String getPlaceOfCMND() {
		return placeOfCMND;
	}

	public void setPlaceOfCMND(String placeOfCMND) {
		this.placeOfCMND = placeOfCMND;
	}

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public int getDelFlg() {
		return delFlg;
	}

	public void setDelFlg(int delFlg) {
		this.delFlg = delFlg;
	}
	
}
