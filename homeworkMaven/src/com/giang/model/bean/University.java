package com.giang.model.bean;

public class University {
	private String id;
	private String site;
	private String code;
	private String name;
	public University() {
		super();
		// TODO Auto-generated constructor stub
	}
	public University(String id, String site, String code, String name) {
		super();
		this.id = id;
		this.site = site;
		this.code = code;
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
